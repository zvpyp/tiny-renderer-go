package model

import (
	"bufio"
	"os"
	"strconv"
	"strings"
)

type Vertex struct {
	X float64
	Y float64
	Z float64
}

type Face struct {
	Verts        [3]int
	TextureVerts [3]int
	NormalVerts  [3]int
}

type Model struct {
	Verts        []Vertex
	TextureVerts []Vertex
	NormalVerts  []Vertex
	Faces        []Face
}

func NewModel(filePath string) (Model, error) {
	var m Model
	objFile, err := os.Open(filePath)
	if err != nil {
		return m, err
	}
	defer objFile.Close()

	lineScanner := bufio.NewScanner(objFile)
	for lineScanner.Scan() {
		line := lineScanner.Text()
		wordScanner := bufio.NewScanner(strings.NewReader(line))
		wordScanner.Split(bufio.ScanWords)
		if wordScanner.Scan() {
			firstWord := wordScanner.Text()

			if firstWord == "v" {
				var newVert Vertex
				wordScanner.Scan()
				newVert.X, err = strconv.ParseFloat(wordScanner.Text(), 64)
				if err != nil {
					return m, err
				}
				wordScanner.Scan()
				newVert.Y, err = strconv.ParseFloat(wordScanner.Text(), 64)
				if err != nil {
					return m, err
				}
				wordScanner.Scan()
				newVert.Z, err = strconv.ParseFloat(wordScanner.Text(), 64)
				if err != nil {
					return m, err
				}
				m.Verts = append(m.Verts, newVert)
			} else if firstWord == "vt" {
				var newVert Vertex
				wordScanner.Scan()
				newVert.X, err = strconv.ParseFloat(wordScanner.Text(), 64)
				if err != nil {
					return m, err
				}
				wordScanner.Scan()
				newVert.Y, err = strconv.ParseFloat(wordScanner.Text(), 64)
				if err != nil {
					return m, err
				}
				wordScanner.Scan()
				newVert.Z, err = strconv.ParseFloat(wordScanner.Text(), 64)
				if err != nil {
					return m, err
				}
				m.TextureVerts = append(m.TextureVerts, newVert)
			} else if firstWord == "vn" {
				var newVert Vertex
				wordScanner.Scan()
				newVert.X, err = strconv.ParseFloat(wordScanner.Text(), 64)
				if err != nil {
					return m, err
				}
				wordScanner.Scan()
				newVert.Y, err = strconv.ParseFloat(wordScanner.Text(), 64)
				if err != nil {
					return m, err
				}
				wordScanner.Scan()
				newVert.Z, err = strconv.ParseFloat(wordScanner.Text(), 64)
				if err != nil {
					return m, err
				}
				m.NormalVerts = append(m.NormalVerts, newVert)
			} else if firstWord == "f" {
				var newFace Face
				// TODO: evitar out of range error si las caras son de 4 vértices o más
				for i := 0; wordScanner.Scan(); i++ {
					vect_str, after, _ := strings.Cut(wordScanner.Text(), "/")
					text_vect_str, after, _ := strings.Cut(after, "/")
					normal_vect_str, _, _ := strings.Cut(after, "/")
					vector_pos, err := strconv.Atoi(vect_str)
					if err != nil {
						return m, err // acá podría mejorar el mensaje de error
					}
					texture_vector_pos, err := strconv.Atoi(text_vect_str)
					if err != nil {
						return m, err // acá también se podría mejorar el mensaje de error xd
					}
					normal_vector_pos, err := strconv.Atoi(normal_vect_str)
					if err != nil {
						return m, err // acá también se podría mejorar el mensaje de error xd
					}
					newFace.Verts[i] = vector_pos - 1
					newFace.TextureVerts[i] = texture_vector_pos - 1
					newFace.NormalVerts[i] = normal_vector_pos - 1
				}
				m.Faces = append(m.Faces, newFace)
			}
		}
	}

	return m, err
}
