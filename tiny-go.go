package main

import (
	"image"
	"image/color"
	"image/draw"
	"image/png"
	"log"
	"math"
	"os"
	"tiny-go/model"
)

type Vector2 struct {
	X int
	Y int
}

type Vector2f struct {
	X float64
	Y float64
}

type Vector3 struct {
	X int
	Y int
	Z int
}

type Vector3f struct {
	X float64
	Y float64
	Z float64
}

type linearT [][]float64

func abs(n int) int {
	if n < 0 {
		return -n
	} else {
		return n
	}
}

func Line(x0, y0, x1, y1 int, img *image.RGBA, tocolor color.RGBA) {
	steep := false
	if abs(x0-x1) < abs(y0-y1) {
		x0, y0, x1, y1 = y0, x0, y1, x1
		steep = true
	}
	if x0 > x1 {
		x0, x1, y0, y1 = x1, x0, y1, y0
	}
	dx := x1 - x0
	dy := y1 - y0
	derror2 := abs(dy) * 2 // this would be 2 * derror * dx
	error2 := 0
	y := y0
	for x := x0; x <= x1; x++ {
		if steep {
			img.Set(y, x, tocolor)
		} else {
			img.Set(x, y, tocolor)
		}
		error2 += derror2
		// notice that if this is error, it would be "if error > 0.5"
		// this is not great because 0.5 is a float
		if error2 > dx {
			if y1 > y0 {
				y += 1
			} else {
				y -= 1
			}
			error2 -= dx * 2 // if this is error, it would be "error -= 1"
		}
	}
}

func WireRender(img *image.RGBA, m *model.Model) {
	halfWidth := float64(img.Bounds().Max.X / 2.0)
	for _, face := range m.Faces {
		for i := 1; i < 3; i++ {
			// notice that if i=2, then v0 is the index 2 vector and v1 the index 0
			v0 := m.Verts[face.Verts[i]]
			v1 := m.Verts[face.Verts[(i+1)%3]]

			// notemos que (0,0) va a estar en el centro de la imagen (por eso sumamos 1.0)
			// y que todo se estira por un factor de halfWidth.
			// Esto tiene sentido porque cada valor (x, y) está entre -1 y 1
			x0 := int((v0.X + 1.0) * halfWidth)
			y0 := int((v0.Y + 1.0) * halfWidth)
			x1 := int((v1.X + 1.0) * halfWidth)
			y1 := int((v1.Y + 1.0) * halfWidth)

			Line(x0, y0, x1, y1, img, color.RGBA{255, 255, 255, 255})
		}
	}
}

func CrossProduct(a, b Vector3) Vector3f {
	i := float64(a.Y)*float64(b.Z) - float64(a.Z)*float64(b.Y)
	j := float64(a.Z)*float64(b.X) - float64(a.X)*float64(b.Z)
	k := float64(a.X)*float64(b.Y) - float64(a.Y)*float64(b.X)
	return Vector3f{i, j, k}
}

func CrossProductFloat(a, b Vector3f) Vector3f {
	i := a.Y*b.Z - a.Z*b.Y
	j := a.Z*b.X - a.X*b.Z
	k := a.X*b.Y - a.Y*b.X
	return Vector3f{i, j, k}
}

func DotProductFloat(a, b Vector3f) float64 {
	return a.X*b.X + a.Y*b.Y + a.Z*b.Z
}

func Normalize(a Vector3f) Vector3f {
	norm := math.Sqrt(DotProductFloat(a, a))
	return Vector3f{a.X / norm, a.Y / norm, a.Z / norm}
}

func DifferenceVector(a, b Vector3f) Vector3f {
	return Vector3f{a.X - b.X, a.Y - b.Y, a.Z - b.Z}
}

func ApplyTranform(T linearT, v Vector3f) Vector3f {
	w := T[3][0]*v.X + T[3][1]*v.Y + T[3][2]*v.Z + T[3][3]
	x := (T[0][0]*v.X + T[0][1]*v.Y + T[0][2]*v.Z + T[0][3]) / w
	y := (T[1][0]*v.X + T[1][1]*v.Y + T[1][2]*v.Z + T[1][3]) / w
	z := (T[2][0]*v.X + T[2][1]*v.Y + T[2][2]*v.Z + T[2][3]) / w
	return Vector3f{x, y, z}
}

func TProduct(T1, T2 linearT) linearT {
	R := make(linearT, 4)
	for i := range R {
		R[i] = make([]float64, 4)
		for j := range R[i] {
			for k := range R[i] {
				R[i][j] += T1[i][k] * T2[k][j]
			}
		}
	}
	return R
}

func IdentityTransform() linearT {
	L := make(linearT, 4)
	for i := range L {
		L[i] = make([]float64, 4)
		L[i][i] = 1
	}
	return L
}

func Translate(e Vector3f) linearT {
	L := IdentityTransform()
	L[0][3] = -e.X
	L[1][3] = -e.Y
	L[2][3] = -e.Z
	return L
}

// the camera is at a z=c
func ZCameraTranform(c float64) linearT {
	L := IdentityTransform()
	L[3][2] = -1 / c
	return L
}

func LookAtTransform(eye, center, up Vector3f) linearT {
	z := Normalize(DifferenceVector(eye, center))
	x := Normalize(CrossProductFloat(up, z))
	y := Normalize(CrossProductFloat(z, x))
	minv := IdentityTransform()
	minv[0][0] = x.X
	minv[0][1] = x.Y
	minv[0][2] = x.Z
	minv[1][0] = y.X
	minv[1][1] = y.Y
	minv[1][2] = y.Z
	minv[2][0] = z.X
	minv[2][1] = z.Y
	minv[2][2] = z.Z
	tr := Translate(eye) // translation

	return TProduct(minv, tr)
}

func Barycentrics(v0, v1, v2, p Vector3f) Vector3f {
	var a1, a2 Vector3f
	{
		// obtain the values of a1 and a2
		AB := Vector2f{v1.X - v0.X, v1.Y - v0.Y}
		AC := Vector2f{v2.X - v0.X, v2.Y - v0.Y}
		PA := Vector2f{v0.X - p.X, v0.Y - p.Y}

		a1 = Vector3f{AC.X, AB.X, PA.X}
		a2 = Vector3f{AC.Y, AB.Y, PA.Y}
	}
	u := CrossProductFloat(a1, a2)
	if math.Abs(u.Z) < 1 {
		// esto solamente se retorna si el triángulo es degenerado
		return Vector3f{-1, 1, 1}
	}
	return Vector3f{1.0 - (u.X+u.Y)/u.Z, u.Y / u.Z, u.X / u.Z}
}

func Triangle(points [3]Vector3f, texturePoints [3]Vector2f, normalPoints [3]Vector3f, img *image.RGBA, texture *image.RGBA, zbuffer []int) {
	// initial values
	bboxmin := Vector2f{points[0].X, points[0].Y}
	bboxmax := Vector2f{points[0].X, points[0].Y}
	width := img.Bounds().Dx() - 1
	textureWidth := texture.Bounds().Dx()
	textureHeight := texture.Bounds().Dy()

	for i := 1; i < 3; i++ {
		if points[i].X < bboxmin.X {
			bboxmin.X = points[i].X
		}
		if points[i].Y < bboxmin.Y {
			bboxmin.Y = points[i].Y
		}
		if points[i].X > bboxmax.X {
			bboxmax.X = points[i].X
		}
		if points[i].Y > bboxmax.Y {
			bboxmax.Y = points[i].Y
		}
	}
	var P Vector3f
	for P.X = math.Round(bboxmin.X); P.X <= bboxmax.X; P.X++ {
		for P.Y = math.Round(bboxmin.Y); P.Y <= bboxmax.Y; P.Y++ {
			bc := Barycentrics(points[0], points[1], points[2], P)
			if bc.X < 0 || bc.Y < 0 || bc.Z < 0 {
				continue
			}
			{
				// calculate Z value of P
				P.Z = points[0].Z * bc.X
				P.Z += points[1].Z * bc.Y
				P.Z += points[2].Z * bc.Z
			}
			if zbuffer[int(P.X)+int(P.Y)*width] < int(P.Z) {
				var pcolor color.RGBA
				var intensity float64
				zbuffer[int(P.X)+int(P.Y)*width] = int(P.Z)
				{
					// calculate colors using barycentrics
					x := bc.X*texturePoints[0].X + bc.Y*texturePoints[1].X + bc.Z*texturePoints[2].X
					y := bc.X*texturePoints[0].Y + bc.Y*texturePoints[1].Y + bc.Z*texturePoints[2].Y
					x *= float64(textureWidth)
					y *= float64(textureHeight)
					x = math.Round(x)
					y = float64(textureHeight) - math.Round(y) // because it's upside down
					pcolor = texture.RGBAAt(int(x), int(y))
					// TODO: change this for a different light source
					var n Vector3f // normal
					light := Vector3f{0, 0, -1.0}
					n.X = bc.X*normalPoints[0].X + bc.Y*normalPoints[1].X + bc.Z*normalPoints[2].X
					n.Y = bc.X*normalPoints[0].Y + bc.Y*normalPoints[1].Y + bc.Z*normalPoints[2].Y
					n.Z = bc.X*normalPoints[0].Z + bc.Y*normalPoints[1].Z + bc.Z*normalPoints[2].Z
					// normalize it, of course
					norm := math.Sqrt(n.X*n.X + n.Y*n.Y + n.Z*n.Z)
					n = Vector3f{n.X / norm, n.Y / norm, n.Z / norm}
					// for some reason the colors are inverted, so I had to multiply this by -1
					intensity = -1 * DotProductFloat(n, light)
					pcolor.R = uint8(float64(pcolor.R) * intensity)
					pcolor.G = uint8(float64(pcolor.G) * intensity)
					pcolor.B = uint8(float64(pcolor.B) * intensity)
					// pcolor.R = uint8(255 * intensity)
					// pcolor.G = uint8(255 * intensity)
					// pcolor.B = uint8(255 * intensity)
				}
				if intensity > 0 {
					img.Set(int(P.X), int(P.Y), pcolor)
				}
			}
		}
	}

}

func OnScreen(points [3]Vector3f, img *image.RGBA) bool {
	w := float64(img.Bounds().Dx() - 1)
	h := float64(img.Bounds().Dy() - 1)
	for _, p := range points {
		if p.X < 0 || p.X > w || p.Y < 0 || p.Y > h {
			return false
		}
	}
	return true
}

func FlatShadingRender(m model.Model, img *image.RGBA, texture *image.RGBA) {
	width := img.Bounds().Dy()
	height := img.Bounds().Dx()
	halfWidth := float64(width) / 2.0
	halfHeight := float64(height) / 2.0
	zbuffer := []int{}
	for i := 0; i <= width*height; i++ {
		zbuffer = append(zbuffer, math.MinInt)
	}
	for _, face := range m.Faces {
		var screenCoords [3]Vector3f
		var worldCoords [3]Vector3f
		var textureCoords [3]Vector2f
		var normalCoords [3]Vector3f
		for j := 0; j < 3; j++ {
			v := m.Verts[face.Verts[j]]
			vf := m.TextureVerts[face.TextureVerts[j]]
			vn := m.NormalVerts[face.NormalVerts[j]]
			x := (v.X + 1.0) * halfWidth
			y := (-v.Y + 1.0) * halfHeight // flipped vertically
			z := (v.Z + 1.0) * halfHeight
			screenCoords[j] = Vector3f{x, y, z}
			worldCoords[j] = Vector3f{v.X, v.Y, v.Z}
			textureCoords[j] = Vector2f{vf.X, vf.Y}
			normalCoords[j] = Vector3f{vn.X, vn.Y, vn.Z}
		}
		// TODO: show triangles with a vertex out of screen
		if OnScreen(screenCoords, img) {
			Triangle(screenCoords, textureCoords, normalCoords, img, texture, zbuffer)
		}
	}
}

func main() {
	img := image.NewRGBA(image.Rect(0, 0, 1000, 1000))
	imgFilename := "test.png"
	f, err := os.Create(imgFilename)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	//make image black
	for i := 0; i <= img.Rect.Max.X; i++ {
		for j := 0; j <= img.Rect.Max.Y; j++ {
			img.Set(i, j, color.RGBA{0, 0, 0, 255})
		}
	}

	m, err := model.NewModel("./obj/african_head.obj")
	if err != nil {
		log.Fatal(err)
	}
	{
		eye := Vector3f{0.6, 0.3, 0.4}
		center := Vector3f{0, 0, 0}
		up := Vector3f{0.2, 1, 0.3}
		modelView := LookAtTransform(eye, center, up)
		// INSERT TRANFORM HERE
		for i, v := range m.Verts {
			m.Verts[i] = model.Vertex(ApplyTranform(modelView, Vector3f{v.X, v.Y, v.Z}))
		}
	}

	var texture *image.RGBA
	{
		textureFile, err := os.Open("./obj/african_head_diffuse.png")
		if err != nil {
			log.Fatal(err)
		}
		defer textureFile.Close()
		src, _, err := image.Decode(textureFile)
		if err != nil {
			log.Fatal(err)
		}
		b := src.Bounds()
		texture = image.NewRGBA(image.Rect(0, 0, b.Dx(), b.Dy()))
		draw.Draw(texture, texture.Bounds(), src, b.Min, draw.Src)
	}

	FlatShadingRender(m, img, texture)
	err = png.Encode(f, img)
	if err != nil {
		log.Fatal(err)
	}

}
